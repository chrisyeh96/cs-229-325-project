from collections import defaultdict
import traci
import numpy as np
import csv
from param_reader import readParams

class SimulationStats(object):
    """Save averagestatistics per second."""
    def __init__(self, params):
        self.name, self.stepLength, self.substepLength = readParams(
            params,
            'SimulationStats',
            [
                ('name', None),
                ('stepLength', None),
                ('substepLength', None)
            ]
        )
        self.substats = defaultdict(list)
        self.stats = defaultdict(float)
        self.keys = ['co2', 'numWaiting', 'distanceTraveled', 'numVehicles']
        self.fp = open('../models/%s/stats.csv' % self.name, 'w')
        self.writer = csv.DictWriter(self.fp, self.keys)
        self.writer.writeheader()
        self.iteration = 0

    def substep(self):
        """Record statistics for a single substep."""
        numWaiting = 0
        co2 = 0
        distanceTraveled = 0
        numVehicles = 0
        for vehicleId in traci.vehicle.getIDList():
            co2 += traci.vehicle.getCO2Emission(vehicleId)
            speed = traci.vehicle.getSpeed(vehicleId)
            if speed < 0.1:
                numWaiting += self.substepLength
            distanceTraveled += speed * self.substepLength
            numVehicles += self.substepLength
        self.substats['co2'].append(co2)
        self.substats['numWaiting'].append(numWaiting)
        self.substats['distanceTraveled'].append(distanceTraveled)
        self.substats['numVehicles'].append(numVehicles)

    def step(self):
        """Write aggregated stats to a file"""
        for key, val in self.substats.items():
            self.stats[key] = np.sum(val) / self.stepLength
        self.substats = defaultdict(list)
        self.writer.writerow(self.stats)
        self.iteration += 1
        if self.iteration % 20 == 0:
            self.fp.flush()
