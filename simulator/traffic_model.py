"""Defines TrafficModel class for generating traffic along multiple routes."""
from __future__ import print_function

from vehicle_generator import VehicleGeneratorPoisson


class TrafficModel(object):
    """Generates routes and traffic along these routes."""

    def __init__(self):
        """Define routes and generators."""
        self.generators = []
        self._generate_routefile()
        self.generators.append(VehicleGeneratorPoisson('right', 'typeWE', 10.))
        self.generators.append(VehicleGeneratorPoisson('left', 'typeWE', 11.))
        self.generators.append(VehicleGeneratorPoisson('down', 'typeNS', 30.))

    def _generate_routefile(self):
        with open("data/cross.rou.xml", "w") as routes:
            print("""
            <routes>
            <vType id="typeWE" accel="0.8" decel="4.5" sigma="0.5" length="5" minGap="2.5" maxSpeed="16.67" guiShape="passenger"/>
            <vType id="typeNS" accel="0.8" decel="4.5" sigma="0.5" length="7" minGap="3" maxSpeed="25" guiShape="bus"/>

            <route id="right" edges="51o 1i 2o 52i" />
            <route id="left" edges="52o 2i 1o 51i" />
            <route id="down" edges="54o 4i 3o 53i" />
            </routes>
            """, file=routes)

    def step(self, time):
        """Buffer generated traffic at the given time."""
        for generator in self.generators:
            generator.step(time)
