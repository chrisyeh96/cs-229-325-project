def readParams(params, name, defaults):
    print(name + ":")
    print("-" * (len(name) + 1))
    out = []
    for key, value in defaults:
        if key in params:
            print("\t%s: %r" % (key, params[key]))
            out.append(params[key])
        else:
            print("\t%s: %r (default)" % (key, value))
            params[key] = value
            out.append(value)
    return out

