import numpy as np
import uuid
import os
import shutil
import json
from collections import defaultdict
import csv
import subprocess
from multiprocessing import Pool
from contextlib import closing

NUM_TO_EVALUATE = 10
NUM_ITERATIONS_TO_AVERAGE = 300

class LogParam:
    def __init__(self, min, max):
        self.minLog = np.log10(min)
        self.maxLog = np.log10(max)

    def generate(self):
        return 10**np.random.uniform(self.minLog, self.maxLog)

class UniformParam:
    def __init__(self, min, max):
        self.min = min
        self.max = max

    def generate(self):
        return np.random.uniform(self.min, self.max)

class ChoiceParam:
    def __init__(self, choices, weights=None):
        self.choices = choices
        if weights is not None:
            self.weights = np.array(weights)
            self.weights = self.weights / np.sum(self.weights)
        else:
            self.weights = None

    def generate(self):
        return np.random.choice(self.choices, p=self.weights)

class SingleParam:
    def __init__(self, value):
        self.value = value

    def generate(self):
        return self.value

choices = {
    'agent': SingleParam('Q'),
    'eta0': LogParam(1e-6, 1e-4),
    'gui': SingleParam(False),
    'batch_size': ChoiceParam([4, 8, 16, 32]),
    'learning_rate': ChoiceParam(['constant', 'invscaling']),
    'alpha': LogParam(0.05, 10),
    'maxIter': SingleParam(500),
    'maxMinutes': SingleParam(20.0),
    'epsilon': UniformParam(.5, 1.0),
    'lqfProb': UniformParam(0.0, 1.0),
    'epsilon_half_life': LogParam(50, 500),
    'gamma': UniformParam(.8, .99),
    'scenario': SingleParam('two_by_two_3lane'),
}


def runModel(modelId):
    """Run the model."""
    pythonExecutable = os.getenv('PYTHON') or 'python'
    ret = subprocess.call([pythonExecutable, 'main.py', modelId])
    if ret != 0:
        raise Exception('subprocess exited with error code: %d' % ret)
    ret = subprocess.call([pythonExecutable, 'main.py', modelId, 'test'])
    if ret != 0:
        raise Exception('subprocess exited with error code: %d' % ret)


def extractStats(modelDir):
    statsFile = os.path.join(modelDir, 'stats.csv')
    data = defaultdict(list)
    with open(statsFile) as f:
        for row in csv.DictReader(f):
            for key in row:
                data[key].append(float(row[key]))
    output = {}
    for key, values in data.items():
        output[key] = np.mean(values[-NUM_ITERATIONS_TO_AVERAGE:])
        output[key + '_std'] = np.std(values[-NUM_ITERATIONS_TO_AVERAGE:])
    return output



def evaluateConfiguration(config):
    trialId = str(uuid.uuid4())
    modelId = os.path.join('hp', trialId)
    modelDir = os.path.join('..', 'models', modelId)
    savePath = os.path.join('..','hp_results', trialId + '.json')
    os.makedirs(modelDir)
    try:
        config_path = os.path.join(modelDir, 'params.json')
        with open(config_path, 'w') as f:
            json.dump(config, f)
        runModel(modelId)
        stats = extractStats(modelDir)
        output = {'config': config, 'stats': stats}
        with open(savePath, 'w') as f:
            json.dump(output, f, indent=2, sort_keys=True)
    except Exception:
        print("Simulation Failed")
        raise
    finally:
        shutil.rmtree(modelDir)


def main():
    configurations = []
    for i in range(NUM_TO_EVALUATE):
        configurations.append(
            dict((key, param.generate()) for key, param in choices.items())
        )
    with closing(Pool(4)) as p:
        p.map(evaluateConfiguration, configurations)

if __name__ == '__main__':
    resultsDir = os.path.join('..', 'hp_results')
    if not os.path.exists(resultsDir):
        os.makedirs(resultsDir)
    main()
