"""Defines Sensor."""
import traci
from collections import deque
import numpy as np
from param_reader import readParams

class Sensor(object):
    """Induction loop sensor.

    Attributes
    ----------
    sensorId: str
    laneId: str
    historyLength: int
    history: list of features collected at each iteration
    iterHistory: list of features collected at each simulation step
    featureKeys: names of the values being captured.
    """

    def __init__(self, sensorId, params):
        """Initialize attributes."""
        self.sensorId = sensorId
        self.laneId = traci.inductionloop.getLaneID(self.sensorId)
        self.historyLength, = readParams(
            params,
            'Sensor',
            [('historyLength', 5)]
        )
        self.history = deque(maxlen=self.historyLength)
        self.iterHistory = []
        self.featureKeys = ['count', 'speed', 'carsWaiting']

    def substep(self):
        """Update running tallies for current simulation step."""
        count = traci.inductionloop.getLastStepVehicleNumber(self.sensorId)
        speed = traci.inductionloop.getLastStepMeanSpeed(self.sensorId)
        carsWaiting = 0
        if count > 0 and speed < 5:
            carsWaiting = 1
        self.iterHistory.append((count, speed, carsWaiting))

    def step(self):
        """Update history for current learning iteration."""
        self.history.append(self._getFeatures())
        self.iterHistory = []
        pass

    def _getFeatures(self):
        """Get features for current time step."""
        count = np.sum([c for c, s, cW in self.iterHistory]) / 2.0
        speed = [s for c, s, cW in self.iterHistory if s >= 0]
        carsWaiting = np.any([cW for _,_,cW in self.iterHistory])
        if len(speed) > 0:
            speed = np.mean(speed) / 20.0
        else:
            speed = 0.
        return [count, speed, carsWaiting]

    def getSchema(self):
        """List of string keys corresponding to the list from getFeatures.

        Returns
        -------
        featureKeys: list of str
            Format <sensorId>_<key>_<timeStep>
            timeStep=0 is the most recent value
        """
        schema = []
        for i in range(self.historyLength):
            for k in self.featureKeys:
                schema.append('%s_%s_%d' % (self.sensorId, k, i))
        return schema

    def getFeatures(self):
        """List of numerical features for this sensor."""
        features = []
        for i in range(self.historyLength):
            if i >= len(self.history):
                features += [0] * len(self.featureKeys)
            else:
                features += self.history[-(i + 1)]
        return features
